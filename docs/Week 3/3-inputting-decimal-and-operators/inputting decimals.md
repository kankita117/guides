# inputting decimals

When user hits the decimal key, a decimal should appear on the display. If user hits any number after hitting a decimal key, the number should be appended on the display as well.

To create this effect, we can concatenate . to the displayed number

add this to your *js* just below your previous edit

```js
if (action === 'decimal') {
  display.textContent = displayedNum + '.'
}
```
Go to the browser and insert the decimal point